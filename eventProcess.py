from multiprocessing import Pool, Process
from multiprocessing import Event

DEFAULT_WAIT_TIMEOUT = 10


class EventProcess(Process):
    def __init__(self, target, close_event):
        super(EventProcess, self).__init__()
        #self.n_workers = n_workers

        if callable(target):
            self.target = target

        self.close_event = close_event

    def run(self):
        if self.target is not None:
            self.target(self.close_event)




