#!/usr/bin/python3.7
# encoding=utf8

"""import psmThingsboardWorker"""
import json
import os
import random
import signal
import socket
import threading
import time

import pika
import logging
from threading import Thread
import paho.mqtt.client as mqtt
import ssl
import base64
import ast

import urllib3

from eventThread import EventThread

ENCODING = 'utf-8'

THINGSBOARD_HOST = "kubemaster.hq.parksmart.it"
THINGSBOARD_PORT = 31312


class PsmThingsboardProducer(object):
    def signal_handler(self, signum, frame):
        """ This function is called when the signal SIGINT is captured and starts the closing procedure """
        print("Handling", signum)
        self.exit()

    def __init__(self, producer_name: str, hostname: str, port: int):
        """ Initialization of the data producer variables that handles mqtt client
        used to connect to Thingsboard server. Two threads are also created:
        data producer thread and verifying connection thread. This thread are closed using "close event" that it's passed
        as a parameter to thread constructor and when the event is set, the thread closes in a cleaner way. """
        signal.signal(signal.SIGINT, self.signal_handler)

        self.exiting = False
        print("[ * ] Producer - Initializing Producer - " + str(producer_name))

        self.lock = threading.Lock()

        self.receivedMessages = []
        self.tasksReplies = []
        self.image = None
        self.imgB64 = None
        self.img_str = None
        self.mqttClient = mqtt.Client(producer_name, clean_session=False)

        self.mqttClient.on_connect = self.on_connect
        self.mqttClient.on_message = self.on_message
        self.mqttClient.on_publish = self.on_publish

        self.connectionStatus = None
        self.isConnecting = False
        self.isConnected = False
        self.errorConnecting = {}

        self.isVerifying = False

        self.producer_name = producer_name

        self.host = hostname
        self.port = port

        self.stop_verify_connection_threads = False
        self.stop_producing_data_threads = False

        self.close_event = threading.Event()

        self.threads = []

        self.verifyConnThread = EventThread(self._verify_connection, self.close_event)
        self.producerThread = EventThread(self._produce_data, self.close_event)

        self.pendingTasks = []

        # self.producer_thread = threading.Thread(target=self.produce_data,
        #                                       args=(lambda: self.stop_producing_data_threads,))

    """def on_disconnect(self, userdata, rc, *flags):
        exit(0)"""

    def on_message(self, client, userdata, msg):
        """
        Callback function called when a message is received through mqtt client that waits for them.
        The only message that will be received are the Consumer confirms after it has completed its task.
        This function extracts params from message payload and, in particular, it extracts the request id
        from params. After that if the request id is among pending tasks, it removes the request id from there.

        :param client: instance of mqtt client
        :param userdata: object containing private user data set when creating client's instance
        :param msg: instance of the message just received
        :return:

        """
        print("[ * ] Producer - " + str(msg.topic))

        msgParams = ast.literal_eval(msg.payload.decode('utf-8')).get("params")
        msgRequestID = ast.literal_eval(str(msgParams)).get("requestID")

        self.lock.acquire()

        self.tasksReplies.append(msg)

        try:
            print("[ * ] Producer - Removing Task " + str(msgRequestID), self.pendingTasks.index(msgRequestID))

            self.pendingTasks.pop(self.pendingTasks.index(msgRequestID))
            print("[ * ] Producer - Removed Task " + str(msgRequestID))
            if self.lock.locked():
                self.lock.release()

        except ValueError:
            if self.lock.locked():
                self.lock.release()
            print("[ * ] Producer - MSG not found among pending tasks")

    def on_publish(self, client, userdata, mid):
        """
        Callback function called when a message is published.
        :param client: instance of mqtt client
        :param userdata: object containing private user data set when creating client's instance
        :param mid: message id
        :return:
        """
        print("[ * ] Producer - Published" + str(mid))
        self.receivedMessages.append(mid)

    def on_connect(self, userdata, rc, *flags):
        """
        Callback function called when connection to mqtt server is completed successfully

        :param userdata: object containing private user data set when creating client's instance
        :param rc: return code
        :param flags: contains some flags regarding connection
        :return:
        """

        print('Connected with result code ' + str(flags[1]))
        """self.mqttClient.publish('v1/devices/me/attributes',
                                json.dumps({"firmware_version": "1.0.2", "serial_number": "SN-001"}),
                                1)"""

        self.mqttClient.subscribe("v1/devices/me/rpc/response/+", qos=2)

        self.connectionStatus = flags[1]

        self.produce_data()

    def verify_connection(self):
        """
        Function that starts the verifying connection thread

        :return:
        """
        """self.lock.acquire()

        self.verifyConnThread = EventThread(self._verify_connection, self.close_event)
        # self.verifyConnThread.setDaemon(True)
        self.lock.release()"""
        self.verifyConnThread.start()

    def _verify_connection(self):
        """
        This function represents the verifying connection thread, in particular its operations.
        It checks the connection status variable and if its value is 0 means that client is connected to the server.
        If it isn't connected, it retries connection until it is connected. If connection is lost, this thread tries
        to reconnect to mqtt server again.

        :return:
        """
        print("[ * ] Producer - Starting verifying connection...")

        self.lock.acquire()

        print("[ * ] Producer - Verifying connection")
        if not self.isVerifying:
            print("[ * ] Producer - Setting verifying flag to true...")
            self.isVerifying = True

        if self.connectionStatus is not None:

            if self.connectionStatus == 0:
                response = os.system("ping -c 1 " + self.host)

                # checking ping response
                if response == 0:
                    self.isConnecting = False
                    self.isConnected = True
                    if self.lock.locked():
                        print("Releasing unlock: (_verify connection")
                        self.lock.release()
                else:
                    self.connectionStatus = None
                    self.isConnected = False
                    if self.lock.locked():
                        print("Releasing unlock: (_verify connection")
                        self.lock.release()

            else:

                if self.connectionStatus == 1:
                    self.errorConnecting = {
                        "errorType": "Connection refused – incorrect protocol version",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 2:
                    self.errorConnecting = {
                        "errorType": "Connection refused – invalid client identifier",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 3:
                    self.errorConnecting = {
                        "errorType": "Connection refused – server unavailable",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 4:
                    self.errorConnecting = {
                        "errorType": "Connection refused – bad credentials (username or password)",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 5:
                    self.errorConnecting = {
                        "errorType": "Connection refused – unauthorized",
                        "errorCode": self.connectionStatus
                    }
                print("[ ! ] Producer - Returned code = " + str(self.connectionStatus))
        elif not self.isConnected:
            print("[ * ] Producer - Connection start...")
            if self.lock.locked():
                print("Releasing unlock: not connected (_verify connection")
                self.lock.release()
            self.connect()
        elif self.lock.locked():
            self.lock.release()

    def connect(self):
        """
        This function handles connection to mqtt server. It starts the verifying connection thread,
        starts mqtt client loop: this loop starts a background thread to process automatically network events.

        :return:
        """

        try:
            self.lock.acquire()
            self.isConnecting = True
            self.isConnected = False
            if not self.isVerifying:
                if self.lock.locked():
                    self.lock.release()
                self.verify_connection()

            self.mqttClient.reconnect_delay_set(1, 60)
            print("[ * ] Producer - Connecting to Thingsboard... 1st try")
            if self.lock.locked():
                self.lock.release()
            self.mqttClient.loop_start()

            self.mqttClient.connect(self.host, self.port)

        except socket.error:
            self.lock.acquire()

            self.isConnecting = False
            self.isConnected = False
            if self.lock.locked():
                print("[ * ] Producer - Releasing lock (socket error)...")
                self.lock.release()
        # except (KeyboardInterrupt, SystemExit):
        # self.exit()

    def setup_client(self):
        """
        Before connection it is necessary to set ssl certificates to connect
        using SSL encryption

        :return:
        """
        print("[ * ] Producer - " + str(self.producer_name) + " - setting up")

        print("[ * ] Producer - Setting TLS Certificates for client authentication...")
        self.mqttClient.tls_set(ca_certs="producerCerts/psm_mqttserver.pub.pem",
                                certfile="producerCerts/psm_mqttclient.nopass.pem",
                                cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLSv1, ciphers=None)
        self.mqttClient.tls_insecure_set(True)

    def produce_data(self):
        """
        Function that starts the producing data thread

        :return:
        """
        """self.lock.acquire()
        self.producerThread = EventThread(self._produce_data, self.close_event)
        self.producerThread.setDaemon(True)
        self.threads.append(self.producerThread)
        self.lock.release()"""

        self.producerThread.start()

    def _produce_data(self):

        """
        This function represents the producing data thread and its operations.
        It loads an image file, encode it as a base 64 string, creates a RPC message to publish with method "new_task" and
        publishes it. Thingsboard waits for RPC Messages with method "new_task" and sends them to Rabbit MQ when received.

        :return:
        """
        self.lock.acquire()
        if self.isConnected:
            print("[ * ] Producer - Loading data from file")
            with open("./CNRPark-Patches-150x150/A/busy/20150703_0805_8.jpg", "rb") as open_file:
                self.image = open_file.read()

            print("[ * ] Producer - Loaded data successfully")

            print("[ * ] Producer - Encoding file using Base64...")
            self.imgB64 = base64.b64encode(self.image)

            print("[ * ] Producer - Decoding file in utf-8")
            self.img_str = self.imgB64.decode(ENCODING)

            print("[ * ] Producer - Decoded file successfully as a string")
            print("[ * ] Producer - Sending new task containing image as string to Thingsboard...")
            requestID = random.randrange(100, 10000000, 1)
            request = {
                "method": "new_task",

                "params": {
                    "requestID": requestID,
                    "image": self.img_str
                }
            }
            print("[ * ] Producer - Task sent successfully")

            try:

                self.mqttClient.publish("v1/devices/me/rpc/request/" + str(requestID), json.dumps(request), qos=2,
                                        retain=True)

                self.mqttClient.publish("v1/devices/me/telemetry",
                                        json.dumps({"last_task": str(requestID)}), qos=2,
                                        retain=True)

                self.pendingTasks.append(requestID)
                if self.lock.locked():
                    self.lock.release()

            except Exception as e:
                if e is isinstance(e, socket.error):
                    print("Net Error: " + str(e))
                elif e is isinstance(e, socket.gaierror):
                    print("GetaddrInfoErr: " + str(e))
                else:
                    print("GenericErr: " + str(e))
                if self.lock.locked():
                    self.lock.release()
        if self.lock.locked():
            self.lock.release()

    def exit(self):
        """
        This function starts the closing procedure where all threads are closed, mqtt client is disconnected and, after
        all that, the main process exits if everything worked fine.

        :return:
        """

        print("[ * ] Producer - Stopping all running threads...")
        # self.lock.acquire()

        self.close_event.set()
        if self.producerThread.is_alive():
            self.producerThread.join()
        if self.verifyConnThread.is_alive():
            self.verifyConnThread.join()

        if self.connectionStatus is 0:
            print("[ * ] Producer - disconnecting mqtt...")
            self.mqttClient.disconnect()

            print("[ * ] Producer - Stopping network loop...")

            self.mqttClient.loop_stop(force=True)

            """if self.lock.locked():
                self.lock.release()"""
            print("[ * ] Producer - Exiting...")

        else:
            """if self.lock.locked():
                self.lock.release()"""
            print("[ * ] Producer - Exiting...")


def main():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        # Legacy Python that doesn't verify HTTPS certificates by default
        pass
    else:
        # Handle target environment that doesn't support HTTPS verification
        ssl._create_default_https_context = _create_unverified_https_context

    psm_producer = PsmThingsboardProducer("PSM-Thingsboard-Producer-1", THINGSBOARD_HOST, THINGSBOARD_PORT)
    psm_producer.setup_client()
    print("[ * ] Producer - Setting connection...")
    psm_producer.connect()


if __name__ == '__main__':
    main()
