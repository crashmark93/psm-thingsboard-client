from threading import Thread
from threading import Event

DEFAULT_WAIT_TIMEOUT = 10


class EventThread(Thread):
    def __init__(self, target, close_event):
        super(EventThread, self).__init__()
        if callable(target):
            self.target = target

        self.close_event = close_event

    def run(self):

        while not self.close_event.isSet():
            self.close_event.wait(DEFAULT_WAIT_TIMEOUT)
            if self.target is not None:
                self.target()

