#!/usr/bin/python3
import ast
import functools
import os

import pika
import json
import random
import time
import threading
import ssl, socket
import urllib3
import logging
import base64
import numpy as np
import cv2 as cv
import paho.mqtt.client as mqtt
from multiprocessing import Event, Queue
from multiprocessing import current_process
from multiprocessing import Pool

from pika.compat import xrange

from eventThread import EventThread

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:

    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context


class PsmThingsboardWorker:
    def __init__(self, hostname, port, mqtt_port, virtual_host, exchange_name, exchange_type, queue_name, shared_queue):
        # logging.debug("[ X ] Setting up connection to RabbitMQ service")

        # self.connection_pool = connection_pool

        # self.workerID = os.getpid()

        self.context = None
        self.ssl_options = None
        self.credentials = None
        self.conn_params = None
        self.connection = None

        self.hostname = hostname
        self.port = port
        self.MQTTport = mqtt_port
        self.virtual_host = virtual_host

        self.connectionStatus = None
        self.isConnecting = False
        self.isConnected = False
        self.errorConnecting = {}

        # self.connectionStatus = {}

        self.channel = None
        self.queue = None
        self.queue_name = queue_name

        self.mqttClient = None

        self.consumer_tag = None

        self.exchange_name = exchange_name
        self.exchange_type = exchange_type

        self.close_event = None

        self.close_thread = threading.Event()

        self.isAlive = False
        self.isClosing = False
        self.consuming = False
        self.was_consuming = False
        self.should_reconnect = False

        self.receivedTasks = []

        self.shared_queue = shared_queue

        self.verifyConnThread = EventThread(self._verify_connection, self.close_thread)
        self.lock = threading.Lock()

    def callback(self, chan, method_frame, _header_frame, body, userdata=None):

        """
            Callback function called when Rabbit MQ client is scheduled to work on a task received from the
            queue "tasks". It gets from the task the request id and the body, putting the body into the shared_queue
            (tasks_ended_queue) and sending to Rabbit MQ the completion ack about that task. After receiving this ack,
            Rabbit MQ sends another task, if present.

            :param chan:
            :param method_frame:
            :param _header_frame:
            :param body:
            :param userdata:
            :return:
        """

        print("[ * ] Worker " + str(os.getpid()) + " received %r" % (body,))

        # self.receivedTasks.append(body)

        msgBody = ast.literal_eval(body.decode("utf-8"))

        msgParams = ast.literal_eval(body.decode('utf-8')).get("params")
        msgRequestID = ast.literal_eval(str(msgParams)).get("requestID")
        msg = body.decode("utf-8")
        self.shared_queue.put(msgBody)


        #print(self.mqttClient.publish("v1/devices/me/rpc/response/" + str(msgRequestID), msg).rc)
        # if self.connectionStatus is 0:

        # self.send_mqtt_rpc_reply(msgRequestID, msg)

        # if self.isConnected:

        chan.basic_ack(delivery_tag=method_frame.delivery_tag)

    def setup_connection(self):
        """
            Function that sets up the connection. In particular sets SSL certificates and passes to the client all
            network parameters needed by pika client to establish a connection to Rabbit MQ server. It sets up a
            Select Connection that is based on callback functions. It tries to establish this connection and, if there
            are no errors, connection is established and on_open callback is called, if not on_open_error callback
            is called and the verifying connection thread tries to reconnect.
            :return:
        """
        if self.should_reconnect:
            self.should_reconnect = False

        self.context = ssl.create_default_context(
            cafile="certs/ca_certificate.pem")
        self.context.load_cert_chain(certfile="certs/client_certificate.pem", keyfile="certs/client_key.pem",
                                     password="CrashMark")

        self.ssl_options = pika.SSLOptions(self.context, server_hostname="kubemaster.hq.parksmart.it")
        self.credentials = pika.credentials.ExternalCredentials()

        self.conn_params = pika.ConnectionParameters(host=self.hostname, virtual_host=self.virtual_host,
                                                     port=self.port, ssl_options=self.ssl_options,
                                                     credentials=self.credentials)

        self.connection = pika.SelectConnection(parameters=self.conn_params,
                                                on_open_callback=self.on_connection_open,
                                                on_open_error_callback=self.on_connection_open_error,
                                                on_close_callback=self.on_connection_closed
                                                )
        print("[ * ] Worker " + str(os.getpid()) + " Trying connecting...")
        try:
            self.connection.ioloop.start()
        except Exception as e:
            print("Error: " + str(e))

        # self.connection = pika.BlockingConnection(self.conn_params)

    def on_connection_open(self, _unused_connection):
        """
        Callback function called when connection to Rabbit MQ server is opened successfully and it starts
        to setup the channell
        :param _unused_connection:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Connected successfully")

        self.setup_channel()

    def on_connection_closed(self, _unused_connection, reason):
        """
            Callback function called when connection to Rabbit MQ server is closed unexpectedly
            :param _unused_connection:
            :param reason
            :return:
        """
        if not self.isAlive:
            self.connection.ioloop.stop()
        else:
            print("[ * ] Worker " + str(os.getpid()) + " Connection closed unexpectedly, reconnecting")
            self.lock.acquire()
            self.should_reconnect = True
            self.lock.release()
            self.exit()

    def on_connection_open_error(self, _unused_connection, error):
        """
        Callback function called when connection to Rabbit MQ Server isn't opened successfully and it starts
        the reconnecting process.
        :param _unused_connection:
        :param error:
        :return:
        """
        print(error)
        self.should_reconnect = True
        self.exit()

    def on_channel_open(self, channel):
        """
        Callback function called when the channel is opened successfully, sets on_channel_close_callback
        and starts to setup the exchange.
        :param channel:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Channel opened successfully")

        self.channel = channel
        self.add_on_channel_close_callback()

        self.setup_exchange()

    def on_channel_closed(self, channel, reason):
        """
        Callback function called when the channel is closed and it closes the connection
        :param channel:
        :param reason:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Channel closed, closing connection")
        self.close_connection()

    def close_connection(self):
        """
        Function that closes connection in a clean way setting consuming flag to false and then closes the connection.
        :return:
        """
        self.consuming = False

        if self.connection.is_closing or self.connection.is_closed:
            print("[ * ] Worker " + str(os.getpid()) + " connection closing or already closed")

        else:

            print("[ * ] Worker " + str(os.getpid()) + " closing connection")
            self.connection.close()

    def on_exchange_declareok(self, _unused_frame, userdata):
        """
        Callback function called when exchange is declared successfully and it starts to setup the queue
        :param _unused_frame:
        :param userdata:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Exchange declared successfully")
        self.setup_queue(self.queue_name)

    def setup_channel(self):
        """
        Function that sets up the channel declaring it in the connection and setting the on_open_callback that is
        called when the channel is open successfully
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Setting channel")
        logging.debug("[ X ] Setting up the channel")
        self.connection.channel(on_open_callback=self.on_channel_open)

        # self.channel.queue_bind(exchange='task_exchange', queue=self.queue_name)
        # self.channel.exchange_declare(exchange='task_exchange', exchange_type='topic', durable=True)

    def setup_exchange(self):
        """
        Function that sets up the exchange, declaring it in the channel declared previously and setting on_exchange_declareok
        callback that is called when exchange is opened successfully.
        :return:
        """
        cb = functools.partial(self.on_exchange_declareok, userdata=self.exchange_name)
        self.channel.exchange_declare(
            exchange=self.exchange_name,
            exchange_type=self.exchange_type,
            durable=True,
            callback=cb)

    def setup_queue(self, queue_name):
        """
        Function that sets up the queue, declaring it in the channel, setting queue_name, durable flag and a callback
        that is called when the queue is declared successfully.
        :param queue_name: name that identifies the queue
        :return:
        """
        cb = functools.partial(self.on_queue_declareok, userdata=queue_name)
        self.queue = self.channel.queue_declare(queue=queue_name, durable=True, callback=cb)
        # self.queue_name = self.queue.method.queue

    def consume(self):
        """
        Function that let client starts consuming queue items. In particular pika client itself passes
        queue elements, when there are some items, to the worker attached to the queue. This function sets a callback
        that is called when an item is received.
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Starting consuming...")

        self.consuming = True
        self.was_consuming = True

        self.consumer_tag = self.channel.basic_consume(queue='tasks', on_message_callback=self.callback)

        print(" [ * ] Worker " + str(os.getpid()) + ' Waiting for messages. To exit press CTRL+C')
        """try:
            self.channel.start_consuming()

        except OSError as ose:
            if ose.errno != 9 and not self.close_event.is_set():
                print(ose)"""

    def reconnect(self):
        """
        This function tries to reconnect the client to Rabbit MQ server
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Reconnecting...")
        self.setup_connection()

    def add_on_channel_close_callback(self):
        """
        Function that sets on_close_callback. This callback function is called when the channel is closed.
        :return:
        """
        self.channel.add_on_close_callback(self.on_channel_closed)

    def add_on_cancel_callback(self):
        """
        This function sets on_cancel_callback function. This callback function is called when the consumer
        is canceled(stopped).
        :return:
        """
        self.channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_queue_declareok(self, _unused_frame, userdata):
        """
        Callback function called when the queue is declared successfully. It binds the queue specified as parameter
        to the exchange specified as parameter.
        :param _unused_frame:
        :param userdata:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Queue declared successfully")
        self.queue_name = userdata
        cb = functools.partial(self.on_bindok, userdata=self.queue_name)
        self.channel.queue_bind(exchange=self.exchange_name, queue=self.queue_name, callback=cb)

    def on_bindok(self, _unused_frame, userdata):
        """
        Callback function called when the binding operation is successful. It sets QOS (Quality of Service) to 1
        in order to send next messages to the worker only if previous messages are acknowledged.
        :param _unused_frame:
        :param userdata:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Queue bound successfully: " + userdata)

        self.set_qos(1)

    def set_qos(self, prefetch_count):
        """
        Function that sets Quality of Service to the one indicated in the parameter "prefetch_count". It is set
        a callback that is called when Quality of Service is set successfully.
        :param prefetch_count:
        :return:
        """

        self.channel.basic_qos(prefetch_count=prefetch_count, callback=self.on_basic_qos_ok)

    def on_basic_qos_ok(self, _unused_frame):
        """
        Callback function called when QOS is set successfully. It calls consume() function that starts consuming
        messages.
        :param _unused_frame:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Quality of service set successfully")
        self.consume()

    def on_consumer_cancelled(self, method_frame):
        """
        Callback function called when consumer is canceled unexpectedly and it closes channel if channel is still up.
        :param method_frame:
        :return:
        """
        print("[ * ] Worker " + str(os.getpid()) + " Consumer was remotely cancelled, shutting down")
        if self.channel:
            self.channel.close()

    def on_cancelok(self, _unused_frame, userdata):
        """
        Callback function called when consumer is normally canceled. It closes channel

        :param _unused_frame:
        :param userdata:
        :return:
        """
        self.consuming = False

        print(
            "[ * ] Worker " + str(os.getpid()) + " Consumer " + userdata + " was cancelled successfully, shutting down")
        self.channel.close()

    def stop_consuming(self):
        """
        Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.
        :return:
        """
        if self.channel:
            cb = functools.partial(
                self.on_cancelok, userdata=self.consumer_tag)
            self.channel.basic_cancel(self.consumer_tag, cb)

    def set_closing(self):
        """
        Function that sets isClosing flag to True
        :return:
        """
        self.isClosing = True

    def exit(self):
        """
        Function that closes everything in a clean way. It checks if isAlive and isClosing flags are True and, if so,
        it sets isAlive flag to False, stops verifyingConnection thread, stops consumer and disconnects mqtt client. The
        stop of the consumer starts the disconnection from Rabbit MQ server

        :return:
        """

        if self.isAlive and self.isClosing:

            print("Disconnecting %r" % (os.getpid()))
            self.isAlive = False

            self.shared_queue.put('STOP')

            self.close_thread.set()
            self.close_event.set()
            if self.verifyConnThread.isAlive():
                self.verifyConnThread.join()
            if self.consuming:
                self.stop_consuming()
            else:
                self.connection.ioloop.stop()

            if self.connectionStatus is 0:
                print("[ * ] Worker " + str(os.getpid()) + " - disconnecting mqtt...")
                self.mqttClient.disconnect()

                print("[ * ] Worker " + str(os.getpid()) + " - Stopping network loop...")

                self.mqttClient.loop_stop(force=True)

                """if self.lock.locked():
                    self.lock.release()"""
                print("[ * ] Worker " + str(os.getpid()) + " - Exiting...")

            # self.connection.close(reply_code=200, reply_text="Normal shutdown")
            print("[ * ] Worker " + str(os.getpid()) + " Disconnected successfully")
        elif self.should_reconnect:
            if self.consuming:
                self.stop_consuming()
            else:
                self.connection.ioloop.stop()

    def _verify_connection(self):
        response = os.system("ping -c 1 " + self.hostname)
        print("[ * ] Worker " + str(os.getpid()) + " Getting lock..")

        self.lock.acquire()

        if self.should_reconnect:
            self.lock.release()
            print("[ * ] Worker " + str(os.getpid()) + " Reconnecting...")
            self.reconnect()
        if self.lock.locked():
            self.lock.release()
            print("[ * ] Worker " + str(os.getpid()) + " Lock released")

    def run(self, close_event):
        print("[ * ] Worker " + str(os.getpid()) + " Running")
        self.isAlive = True
        self.close_event = close_event
        print("[ * ] Worker " + str(os.getpid()) + " Setting connection ...")
        self.verifyConnThread.start()
        self.setup_connection()

        # self.setup_channel()
        # self.consume()

        """if img is None:
            logging.error("[ ! ] ERROR: Initialization failed, missing data to be processed by the worker")
            exit(-1);
        self.img = base64.b64decode(img);
        self.imgRes = None;"""

    def send_mqtt_rpc_reply(self, request_id, msg):
        print("[ * ] Worker " + str(os.getpid()) + " - Task " + str(request_id) +
              " completed, sending reply to producer")

        print("Published")

    """def execute_job(self):
        logging.debug("[ * ] Beginning to execute the job")"""
