import json
import multiprocessing
import os
import signal
import socket
import ssl
import threading

from pika.compat import xrange

import psmThingsboardWorker
import pika
import logging
import paho.mqtt.client as mqtt
from multiprocessing import Event, Lock, Queue, Pool
from eventProcess import EventProcess
from eventThread import EventThread
from psmThingsboardWorker import PsmThingsboardWorker
from kombu import Connection, Exchange, Queue, pools


class PsmThingsboardConsumer:

    def __init__(self, n_workers, hostname, port, mqtt_port, virtual_host, exchange_name, exchange_type, queue_name, queue):

        self.n_workers = n_workers

        self.hostname = hostname
        self.port = port
        self.mqtt_port = mqtt_port
        self.virtual_host = virtual_host
        self.exchange_name = exchange_name
        self.exchange_type = exchange_type
        self.queue_name = queue_name

        self.workers = []
        self.processes = []
        self.close_event = Event()

        self.setup_completed = False

        self.isConnecting = False
        self.isConnected = False

        self.lock = Lock()

        self.threadLock = threading.Lock()
        self.close_thread = threading.Event()

        self.verifyConnThread = EventThread(self._verify_mqtt_connection, self.close_thread)

        self.mqttClient = None

        self.tasks_exchange = None
        self.tasks_queue = None
        self.connection = None
        self.connectionPool = None

        self.tasks_ended_queue = queue
        #self.pool = Pool()

        self.connectionStatus = None

    def _verify_mqtt_connection(self):
        """
                This function represents the verifying connection thread, in particular its operations.
                It checks the connection status variable and if its value is 0 means that mqtt client is connected to the server.
                If it isn't connected, it retries connection until it is connected. If connection is lost, this thread tries
                to reconnect to mqtt server again. This function checks even if the server is reachable and if it isn't,
                it considers the connection as lost and starts the reconnecting procedure.

                :return:
                """
        self.threadLock.acquire()
        response = os.system("ping -c 1 " + self.hostname)

        if self.connectionStatus is not None:

            if self.connectionStatus == 0:

                # checking ping response
                if response is 0:
                    print("[ * ] Worker " + str(os.getpid()) + " mqtt still connected")
                    self.isConnecting = False
                    self.isConnected = True
                    if self.threadLock.locked():
                        print("[ * ] Worker " + str(os.getpid()) + " mqtt Releasing unlock: (_verify connection")
                        self.threadLock.release()
                else:
                    self.connectionStatus = None

                    self.isConnecting = False
                    self.isConnected = False

                    if self.threadLock.locked():
                        print("[ * ] Worker " + str(os.getpid()) + " mqtt Releasing unlock: (_verify connection")
                        self.threadLock.release()

            else:
                if self.connectionStatus == 1:
                    self.errorConnecting = {
                        "errorType": "Connection refused – incorrect protocol version",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 2:
                    self.errorConnecting = {
                        "errorType": "Connection refused – invalid client identifier",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 3:
                    self.errorConnecting = {
                        "errorType": "Connection refused – server unavailable",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 4:
                    self.errorConnecting = {
                        "errorType": "Connection refused – bad credentials (username or password)",
                        "errorCode": self.connectionStatus
                    }
                elif self.connectionStatus == 5:
                    self.errorConnecting = {
                        "errorType": "Connection refused – unauthorized",
                        "errorCode": self.connectionStatus
                    }
                print("[ ! ] Worker " + str(os.getpid()) + " - mqtt Returned code = " + str(self.connectionStatus))
        elif not self.isConnected and not self.isConnecting:
            print("[ * ] Worker " + str(os.getpid()) + " - mqtt Connection start...")
            if self.threadLock.locked():
                print("[ ! ] Worker " + str(os.getpid()) + " mqtt Releasing unlock: not connected (_verify connection")
                self.threadLock.release()
            self.connect_mqtt_client()
        elif self.threadLock.locked():
            self.threadLock.release()

    def on_message(self, client, userdata, msg):
        """
               Callback function called when a message is received through mqtt client that waits for them.
               The only message that will be received are the Consumer confirms after it has completed its task.
               This function extracts params from message payload and, in particular, it extracts the request id
               from params. After that if the request id is among pending tasks, it removes the request id from there.

               :param client: instance of mqtt client
               :param userdata: object containing private user data set when creating client's instance
               :param msg: instance of the message just received
               :return:

               """
        print("[ * ] Worker " + str(os.getpid()) + " - " + str(msg))
        # self.tasksReplies.append(msg)

    def on_publish(self, client, userdata, mid):
        """
               Callback function called when a message is published.
               :param client: instance of mqtt client
               :param userdata: object containing private user data set when creating client's instance
               :param mid: message id
               :return:
               """
        print("[ * ] Worker " + str(os.getpid()) + " - published " + str(mid))
        # self.receivedMessages.append(mid)

    def on_connect(self, client, userdata, flags, rc, properties=None):
        """
               Callback function called when connection to mqtt server is completed successfully

               :param client: mqtt client instance
               :param userdata: object containing private user data set when creating client's instance
               :param rc: return code
               :param flags: contains some flags regarding connection
               :return:
               """
        print("[ * ] Worker " + str(os.getpid()) + ' Connected to MQTT with result code ' + str(rc) + str(
            flags['session present']))

        print("[ * ] Worker " + str(os.getpid()) + " Getting lock..")
        self.threadLock.acquire()
        self.connectionStatus = rc
        self.isConnecting = False
        self.isConnected = True

        print("[ * ] Worker " + str(os.getpid()) + " Set conneccted flags")

        if self.threadLock.locked():
            print("[ * ] Worker " + str(os.getpid()) + " Releasing lock..")
            self.threadLock.release()
        # self.mqttClient.publish('v1/devices/me/attributes',
        #                       json.dumps({"firmware_version": "1.0.2", "serial_number": "SN-001"}),
        #                      1)

        # self.connectionStatus = flags[1]

        # self.produce_data()

    def create_mqtt_client(self):
        """
                This function sets up the MQTT Client passing SSL certificates,
                setting callback functions (on_connect, on_publish, etc...). After that it starts
                connection to Thingsboard server

                :return:

                """
        self.mqttClient = mqtt.Client("worker-" + str(os.getpid()), clean_session=False)

        self.mqttClient.on_connect = self.on_connect
        # self.mqttClient.on_message = self.on_message
        self.mqttClient.on_publish = self.on_publish

        print("[ * ] Worker " + str(os.getpid()) + " - MQTT setting up")

        print("[ * ] Worker " + str(os.getpid()) + " - Setting TLS Certificates for client MQTT authentication...")
        self.mqttClient.tls_set(ca_certs="workerCerts/psm_mqttserver.pub.pem",
                                certfile="workerCerts/psm_mqttclient.nopass.pem",
                                cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLSv1, ciphers=None)
        self.mqttClient.tls_insecure_set(True)

        self.connect_mqtt_client()

    def connect_mqtt_client(self):
        """
        This function starts the connection to MQTT server in Thingsboard. This connection is necessary to
        let the consumer sends the answewr about the task to the contributor. If connection fails, it retries connection
        and the verifying connection thread tries to ping the main server and if it fails, it retries after a bit of
        seconds.

        :return:

        """
        self.threadLock.acquire()
        self.isConnecting = True
        self.isConnected = False

        self.mqttClient.reconnect_delay_set(1, 60)
        print("[ * ] Worker " + str(os.getpid()) + " - Connecting to Thingsboard... 1st try")

        if self.threadLock.locked():
            self.threadLock.release()

        try:
            self.mqttClient.loop_start()
            self.mqttClient.connect(self.hostname, self.mqtt_port)
            # self.mqttClient.loop_forever()

        except socket.error:
            self.threadLock.acquire()
            self.isConnecting = False
            self.isConnected = False
            if self.threadLock.locked():
                print("[ * ] Worker " + str(os.getpid()) + " - Releasing lock (socket error)...")
                self.threadLock.release()
        except Exception as e:
            print("Error: " + str(e))

    """def setup_main_client(self):
        self.tasks_exchange = Exchange('task_exchange', 'topic', durable=True)
        self.tasks_queue = Queue('tasks', exchange=self.tasks_exchange, routing_key='tasks')

        self.connection = Connection(hostname=self.hostname,
                                     port=self.port,
                                     virtual_host=self.virtual_host,
                                     transport='amqp',
                                     login_method='EXTERNAL',
                                     ssl={
                                         'ca_certs': 'certs/ca_certificate.pem',
                                         'keyfile': 'certs/client_key.pem',
                                         'certfile': 'certs/client_certificate.pem',
                                         'cert_reqs': ssl.CERT_REQUIRED,
                                     })

        print("Connection setup")
        try:

            self.connectionPool = self.connection.Pool(limit=self.n_workers)
        except Exception as e:
            print(e)"""

    def get_processes(self):
        return self.processes

    def signal_sigint_handler(self, signum, frame):
        print("Handling SIGINT, exiting...")
        self.exit()

    def setup_consumers(self):
        """
            Function that instantiates n workers where each worker is created as a separated process.
            It is created an array containing all thread handlers. At the end, after having started all workers,
            the queue "tasks_ended_queue" it is checked and while it doesn't contain the string "STOP" the function
            continues to check if there are some ended tasks and, if it's so, it gives the feedback to Thingsboard about
            its completion.


            :return:

        """

        for i in range(self.n_workers):
            self.workers.append(PsmThingsboardWorker(self.hostname, self.port, self.mqtt_port, self.virtual_host,
                                                     self.exchange_name, self.exchange_type, self.queue_name,
                                                     self.tasks_ended_queue))
            self.processes.append(EventProcess(self.workers[i].run, self.close_event))
            self.processes[i].start()

        for task in iter(self.tasks_ended_queue.get, 'STOP'):
            print("Task completed: ", str(task.get('params').get('requestID')))
            self.mqttClient.publish("v1/devices/me/rpc/response/" + str(task.get('params').get('requestID')))
            self.mqttClient.publish("v1/devices/me/telemetry", json.dumps({"last_task": task.get('params').get('requestID')}))

    def exit(self):
        """
            This function starts the closing procedure setting the close_thread event which closes all threads and,
            after that it starts each worker's closing procedure. After that the main thread simply exits because there
            aren't anymore instructions to execute.
            :return:
        """
        print("Closing workers..")

        self.close_thread.set()
        for worker in self.workers:

            if worker.isAlive:
                print("Closing worker %r" % worker)
                worker.set_closing()
                worker.exit()


def main():
    queue = multiprocessing.Queue()
    consumer = PsmThingsboardConsumer(7, "kubemaster.hq.parksmart.it", 31816, 31312, "/psm", "task_exchange", "topic",
                                      "tasks", queue)
    signal.signal(signal.SIGINT, consumer.signal_sigint_handler)

    consumer.create_mqtt_client()
    consumer.verifyConnThread.start()
    consumer.setup_consumers()


if __name__ == '__main__':
    main()
