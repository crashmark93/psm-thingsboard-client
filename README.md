# THINGSBOARD SETUP

## Requirements
1. CentOS/RHEL host (suggested if you use Ansible scripts, 
    if not every OS is fine)
2. Minimum 9GB RAM
3. CPU that supports virtualization

## Dependencies

CentOS/RHEL users can use the Ansible script 
`./psm-thingsboard/cluster-setup/kube-dependencies.yml`.
The Ansible script installs docker (required by Kubernetes), 
enables all required yum repositories and installs
all required packages: kubeadm, kubelet, kubectl.
After that configures firewall to open some required ports,
enables br_netfilter kernel module and disables SELINUX.
Other OS users can follow the official documentation 
related to Kubernetes setup.

## Kubernetes cluster setup

CentOS/RHEL users can use the Ansible script
`./cluster-setup/kube-master_setup.yml`.
This script cleans kubernetes removing old clusters if there are some and,
after that initializes a new cluster creating even the pod network.

## Thingsboard cluster setup

In order to install and configure Thingsboard cluster, it's needed 
to be executed `./psm-thingsboard/k8s-install-tb.sh` shell script.
After that it's needed to be launched `./psm-thingsboard/k8s-deploy-resources.sh`.
The first script starts creating the namespace **thingsboard**
that will be used to install all thingsboard's cluster. It installs and configures cassandra-db
as database that will be used by thingsboard's microservices.
The second script deploys all thingsboard microservices.
Each micro service exposes a port to which one or more clients can connect
and in this way use the service.

## Thingsboard Uninstall

In order to uninstall Thingsboard there 2 scripts:
1. `./psm-thingsboard/k8s-delete-resources.sh`: this script removes only the deployed resources (micro services)
    leaving database and its storage untouched;
2. `./psm-thingsboard/k8s-delete-all.sh`: this script removes everything related
    to Thingsboard.

## Rabbit MQ setup
First of all it's required to launch `./psm-rabbitmq/get_helm.sh` script to install helm in the cluster. 
After that it's needed to be executed the following command in the directory ./psm-rabbitmq:

    helm install --name psm -f rabbitmq-conf.yaml stable/rabbitmq-ha

and then:
    
    kubectl apply -f storage-config.yaml
    
It installs rabbitmq in the kubernetes cluster with high availability options enabled.
It installs also SSL Certificates required to setup a SSL connection towards itself.
It's possible to use tls-gen, located in `./psm-rabbitmq/tls-gen`, to generate 
new certificates)

## Rabbit MQ Uninstall

In order to uninstall Rabbit MQ `psm` release it's needed to be executed the following
command: 

    helm delete psm

 


 